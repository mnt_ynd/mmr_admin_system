package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.RentalDAO;
import jp.mnt.dao.StaffDAO;
import jp.mnt.dto.StaffDTO;
import jp.mnt.exception.SystemError;

/**
 * login.jspからsubmitされた値をチェックして遷移する
 */

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null && session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
		}

		// ログイン済み
		Integer id = (Integer) session.getAttribute("staff_id");
		Integer manegerFLG = (Integer) session.getAttribute("manager_flg");
		if (id != null && manegerFLG != null) {
			response.sendRedirect("rental");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		// Postリクエストを得る
		String mail = request.getParameter("mail");
		String password = request.getParameter("pass");

		// 入力チェックを行い不適切だとerror_messageをlogiin.jspに送る
		String check = loginCheck(mail, password);
		if (!check.equals("")) {
			request.setAttribute("error_message", check);
			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
			return;
		}

		// IDとパスワードから従業員をチェック
		try (Connection conn = DataSourceManager.getConnection()) {

			// idとpasswordの検索
			StaffDAO dao = new StaffDAO(conn);
			ArrayList<StaffDTO> list = dao.selectByIdAndPassword(mail, password);

			// 従業員が１件見つからなかったとき、error_messageをlogiin.jspに送る
			if (list == null || list.size() == 0) {
				request.setAttribute("error_message", "ユーザIDまたはパスワードが間違っています");
				request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
				return;
			}

			// 合致する従業員が２件以上ある
			if (list.size() >= 2) {
				request.setAttribute("error_message", "管理者にお問い合わせください。");
				request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
				return;
			}

			// trueの時error_messageをリセット
			request.removeAttribute("error_message");

			// 従業員情報をセッションスコープにセット
			session.setAttribute("staff_id", list.get(0).getStaffId());
			session.setAttribute("staff_name", list.get(0).getLastName() + " " + list.get(0).getFirstName());
			session.setAttribute("manager_flg", list.get(0).getManagerFlg());

			// 毎日１回延滞に変更する処理(その日一番最初にログインしたユーザー)
			changeDelay(conn);

			// パスワードが初期値(staff)の時パスワード変更画面に遷移する
			if (password.equals("staff")) {
				request.getRequestDispatcher("/WEB-INF/jsp/passwordChange.jsp").forward(request, response);
				return;
			}

			// レンタル一覧へ遷移
			request.getRequestDispatcher("rental").forward(request, response);
		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("login");
			return;
		}
	}

	public void changeDelay(Connection conn) {
		// 延滞の処理
		// アプリケーションスコープ
		ServletContext sc = getServletContext();

		// 今日の日付を取得
		Calendar tody = Calendar.getInstance();
		// スコープの値が入っていない(初回起動)、又は値と今日の日付が同じでない(=今日はまだ処理を行っていない)場合は延滞処理を行う
		if (sc.getAttribute("delay_batch_date") == null
				|| ((Calendar) sc.getAttribute("delay_batch_date")).equals(tody) == false) {

			// 遅延に変更
			RentalDAO rentalDAO = new RentalDAO(conn);
			System.out.println(rentalDAO.changeDely(rentalDAO.countDely()) + "件を延滞に更新");
			// 処理日を上書き
			sc.setAttribute("delay_batch_date", tody);
		}
	}

	/**
	 * ログインIDとパスワードの入力チェックを行う
	 */
	public String loginCheck(String mail, String pass) {

		String check = "";

		// メールアドレス検査用正規表現
		String mailFormat = "^[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+(\\.[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+)*+(.*)@[a-zA-Z0-9][a-zA-Z0-9\\-]*(\\.[a-zA-Z0-9\\-]+)+$";
		if (!mail.matches(mailFormat)) {
			check = "ログインIDの形式が正しくありません。";
			return check;
		}

		// passに半角英数字以外があるとエラー文を返す
		for (int i = 0; i < pass.length(); i++) {
			char c = pass.charAt(i);
			if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
				check = "パスワードには半角英数字のみ入力してください";
				return check;
			}
		}
		return check;
	}
}
