package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.Validate;
import jp.mnt.dao.StaffDAO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class DeleteEmpServlet
 */
@WebServlet("/deleteEmp")
public class DeleteEmpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
		}

		// 管理者なら従業員マスタページへ
		if (managerFLG == 1) {
			response.sendRedirect("empMaster");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// Postリクエストを得る
		request.setCharacterEncoding("UTF-8");
		String[] inputDeleteStaffId = request.getParameterValues("deleteId");

		// TODO もしチェックボックスがどれも選択されていなかったら

		// 値の入力チェック
		for (String string : inputDeleteStaffId) {
			if (Validate.isNum(string) == false) {
				request.setAttribute("delete_staff_message", "値が不正です。");
				response.sendRedirect("empMaster");
			}
		}

		// 従業員（論理）削除機能
		try (Connection conn = DataSourceManager.getConnection()) {

			// DAO
			StaffDAO staffDAO = new StaffDAO(conn);

			// オートコミットをオフ
			conn.setAutoCommit(false);

			// 実行
			int result = 0;
			for (String deleteStaffId : inputDeleteStaffId) {
				result += staffDAO.deteleStaff(deleteStaffId);
			}

			// コミット
			conn.commit();
			conn.setAutoCommit(true);

			// リクエストスコープに設定
			if (result >= 1) {
				request.setAttribute("delete_staff_message", result + "件の削除が完了しました。");
			} else {
				request.setAttribute("delete_staff_message", "削除に失敗しました。");
			}

			// 従業員マスタメンテナンスへ遷移
			request.getRequestDispatcher("empMaster").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("empMaster");
			return;
		}
	}

}
