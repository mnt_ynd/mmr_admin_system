package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.Validate;
import jp.mnt.dao.StaffDAO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class ResetEmpPasswordServlet
 */
@WebServlet("/resetEmpPass")
public class ResetEmpPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// Getリクエストを得る
		request.setCharacterEncoding("UTF-8");
		String inputStaffId = request.getParameter("staffId");

		// 入力エラーチェックに引っかかったら遷移
		if (Validate.isNum(inputStaffId) == false) {
			session.setAttribute("reset_staff_message", "従業員番号が正しくありません。");
			response.sendRedirect("empMaster");
			return;
		}

		// 従業員パスワードリセット機能
		try (Connection conn = DataSourceManager.getConnection()) {
			// DAO
			StaffDAO staffDAO = new StaffDAO(conn);

			// SQL実行
			int result = staffDAO.resetStaffPassword(inputStaffId);

			// リクエストスコープに設定
			if (result == 1) {
				session.setAttribute("reset_staff_message", inputStaffId + "のパスワードリセットが完了しました。");
			} else {
				session.setAttribute("reset_staff_message", inputStaffId + "のパスワードリセットに失敗しました。");
			}

			// 従業員マスタメンテへ遷移
			response.sendRedirect("empMaster");
		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("empMaster");
			return;
		}
	}
}
