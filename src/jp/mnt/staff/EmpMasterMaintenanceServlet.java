package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.StaffDAO;
import jp.mnt.dto.StaffDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class EmpMasterMaintenanceServlet
 */
@WebServlet("/empMaster")
public class EmpMasterMaintenanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// コネクション
			StaffDAO staffDAO = new StaffDAO(conn);

			// SQLを実行
			ArrayList<StaffDTO> staffList = staffDAO.selectAllStaffExceptDeleteStaff();

			// リクエストスコープに設定
			request.setAttribute("staff_list", staffList);

			// スタッフMaintenanceへ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/employeeMaintenance.jsp").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("empMaster");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
