package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.Validate;
import jp.mnt.dao.StaffDAO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class RepairEmpServlet
 */
@WebServlet("/repairEmp")
public class RepairEmpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 管理者なら従業員マスタページへ
		if (managerFLG == 1) {
			response.sendRedirect("empMaster");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// Postリクエストを得る
		request.setCharacterEncoding("UTF-8");
		String repairStaffId = request.getParameter("sbStaffId");
		String repairLastName = request.getParameter("sbLastName");
		String repairFirstName = request.getParameter("sbFirstName");
		String repairManagerFLG = request.getParameter("sbManagerFLG");

		String[] insertStaff = { repairStaffId, repairLastName, repairFirstName, repairManagerFLG };

		List<String> error = new ArrayList<>();

		// スタッフIDの入力チェック
		if (Validate.isNum(repairStaffId) == false) {
			error.add("スタッフIDが正しくありません");
		}
		// 苗字の入力チェック
		if (Validate.isName(repairLastName) == false) {
			error.add("苗字の入力が正しくありません。");
		}
		// 名前の入力チェック
		if (Validate.isName(repairFirstName) == false) {
			error.add("名前の入力が正しくありません。");
		}
		// 権限の入力チェック
		if (Validate.isManagerFLG(repairManagerFLG) == false) {
			error.add("権限の選択が正しくありません。");
		}

		// 入力エラーが1件でもあれば返す
		if (error.size() >= 1) {
			session.setAttribute("repair_staff_message", error);
			response.sendRedirect("empMaster");
			return;
		}

		// 従業員編集機能
		try (Connection conn = DataSourceManager.getConnection()) {
			// DAO
			StaffDAO staffDAO = new StaffDAO(conn);

			// DTO
			int result = staffDAO.updateStaff(insertStaff);

			// リクエストスコープに設定
			if (result == 1) {
				session.setAttribute("repair_staff_message", repairStaffId + "の変更が完了しました。");
			} else {
				session.setAttribute("repair_staff_message", repairStaffId + "の変更できませんでした。");
			}

			// 従業員マスタメンテへ遷移
			response.sendRedirect("empMaster");
		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("empMaster");
			return;
		}
	}

}
