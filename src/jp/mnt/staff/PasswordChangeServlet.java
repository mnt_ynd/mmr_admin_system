package jp.mnt.staff;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.PasswordDAO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class PasswordChangeServlet
 */
@WebServlet("/passwordChange")
public class PasswordChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		// String staffName = (String) session.getAttribute("staff_name");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 管理者ならば従業員マスタへ
		if (managerFLG == 1) {
			response.sendRedirect("empMaster");
			return;
		}
	}

	protected void doReturn(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/jsp/passwordChange.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			doGet(request, response);
			return;
		}
		if ("".equals(session.getAttribute("staff_id"))) {
			doGet(request, response);
			return;
		}

		request.setCharacterEncoding("UTF-8");
		String password1 = request.getParameter("pass1");
		String password2 = request.getParameter("pass2");
		String strength = request.getParameter("strength");
		Integer staffId = (Integer) session.getAttribute("staff_id");

		// password1とpassword2の入力チェックを行う
		String check = passwordCheck(password1, password2);
		if (!check.equals("")) {
			request.setAttribute("error_message", check);
			doReturn(request, response);
			return;
		}

		if ("弱い".equals(strength)) {
			request.setAttribute("error_message2", "パスワードが簡単すぎます。");
			doReturn(request, response);
			return;
		}
		// コネクションの取得

		Connection conn = null;
		try {
			conn = (Connection) DataSourceManager.getConnection();

			PasswordDAO dao = new PasswordDAO(conn);
			dao.updatePassword(staffId, password1);

			// trueの時RentalServletにフォワード
			request.getRequestDispatcher("rental").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("empMaster");
			return;
		}

	}

	/**
	 * パスワードとパスワード（確認）の入力チェックを行う
	 */
	public String passwordCheck(String pass1, String pass2) {

		String check = "";

		if (pass1.length() <= 5) {
			check = "文字数が少なすぎます";
			return check;
		}

		// // pass1に半角英数字以外があるとエラー文を返す
		// for (int i = 0; i < pass1.length(); i++) {
		// char c = pass1.charAt(i);
		// if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c >
		// 'Z')) {
		// check = "パスワードには半角英数字のみ入力してください";
		// return check;
		// }
		// }
		//
		// // pass2に半角英数字以外があるとエラー文を返す
		// for (int i = 0; i < pass2.length(); i++) {
		// char c = pass2.charAt(i);
		// if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c >
		// 'Z')) {
		// check = "パスワードには半角英数字のみ入力してください";
		// return check;
		// }
		// }

		// 空文字は登録できないように
		if ("".equals(pass1)) {
			check = "パスワードは必ず入力してください。";
			return check;
		}

		// staffは登録できないように
		if ("staff".equals(pass1)) {
			check = "「staff」は登録できないパスワードです。";
			return check;
		}

		// pasu1とpass2が違うとエラー文を返す
		if (!pass1.equals(pass2)) {
			check = "パスワードとパスワード（確認）が違います";
			return check;
		}
		return check;
	}

}
