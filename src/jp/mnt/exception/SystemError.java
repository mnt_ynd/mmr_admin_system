package jp.mnt.exception;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;

@SuppressWarnings("serial")
public class SystemError extends RuntimeException {

	public SystemError(SQLException e) {
		super(e);
	}

	public SystemError(NamingException e) {
		super(e);
	}

	public SystemError(IOException e) {
		super(e);
	}

	public SystemError(Exception e) {
		super(e);
	}

	public SystemError(NullPointerException e) {
		super(e);
	}
}
