package jp.mnt.exception;

import java.sql.SQLException;

import javax.naming.NamingException;

@SuppressWarnings("serial")
public class DataSourceException extends RuntimeException {

	public DataSourceException(SQLException message) {
		super(message);
	}

	public DataSourceException(NamingException message) {
		super(message);
	}

	public DataSourceException(Exception message) {
		super(message);
	}
}
