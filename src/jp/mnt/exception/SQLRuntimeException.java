package jp.mnt.exception;

import java.sql.SQLException;

@SuppressWarnings("serial")
public class SQLRuntimeException extends RuntimeException {

	public SQLRuntimeException(SQLException message) {
		super(message);
	}
}
