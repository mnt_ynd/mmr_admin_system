package jp.mnt.item;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.imageio.ImageIO;
import javax.naming.NamingException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.ItemDAO;

/**
 * Servlet implementation class GetImageServlet
 */
@WebServlet("/getimage")
public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try (Connection conn = DataSourceManager.getConnection()) {
			ItemDAO dao = new ItemDAO(conn);

			String numStr = req.getParameter("id");
			int itemId = Integer.parseInt(numStr);
			BufferedImage img = dao.selectImageById(itemId);

			// 画像をクライアントに返却する
			res.setContentType("image/jpeg");
			OutputStream os = res.getOutputStream();
			if (img != null) {
				ImageIO.write(img, "jpg", os);
			}
			os.flush();
		} catch (SQLException | NamingException | NullPointerException e) {
			res.sendRedirect("error.jsp");
		} catch (NumberFormatException e) {
			res.sendRedirect("error.jsp");
		} catch (Exception e) {
			res.sendRedirect("error.jsp");
		}
	}
}