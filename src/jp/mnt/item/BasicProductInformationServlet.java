package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.ItemDAO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.SearchDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class BasicProductInformationServlet
 */
@WebServlet("/basicProductInformation")
public class BasicProductInformationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 管理者なら商品マスタ一覧へ
		if (managerFLG == 1) {
			response.sendRedirect("item");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session == null) {
			doGet(request, response);
			return;
		}
		if ("".equals(session.getAttribute("staff_id"))) {
			doGet(request, response);
			return;
		}
		request.setCharacterEncoding("UTF-8");
		String searchString = request.getParameter("item");
		String category = request.getParameter("category");
		String genre = request.getParameter("genre");

		// 文字が入力されなかったとき商品を表示しない
		String message = "";
		if ("".equals(searchString)) {
			message = "タイトルを入力してください";
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);
			return;
		}

		SearchDTO searchList = new SearchDTO();
		searchList.setSearchString(searchString);
		searchList.setCategory(category);
		searchList.setGenre(genre);
		session.setAttribute("searchList", searchList);

		// コネクションの取得
		// listにDAOで取ったデータを保持
		Connection conn = null;
		ArrayList<ItemDTO> list;
		int count = 0;
		try {
			conn = DataSourceManager.getConnection();
			ItemDAO dao = new ItemDAO(conn);

			// カテゴリ、ジャンルが選択されているか判定
			if (category.equals("0")) {
				list = dao.selectText(searchString);
				count = dao.selectTextCount(searchString);
			} else if (genre.equals("0")) {
				list = dao.selectTextAndCategory(searchString, category);
				count = dao.selectTextCategoryCount(searchString, category);
			} else {
				list = dao.selectTextAndCategoryAndGenre(searchString, category, genre);
				count = dao.selectTextCategoryAndGenreCount(searchString, category, genre);
			}
			// 見つかった件数を送る
			request.setAttribute("ItemCount", count);
			// 見つかった情報をリストで送る
			request.setAttribute("ItemList", list);
			message = "";
			if (list.size() == 0) {
				message = "検索文字列に該当するデータは存在しません";
			}
			request.setAttribute("message", message);
			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);

		}catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("item");
			return;
		}

	}

}
