package jp.mnt.item;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.ItemDAO;
import jp.mnt.dto.InsertItemDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class RegitItemServlet
 */
@MultipartConfig(location = "")
@WebServlet("/regitItem")
public class RegitItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 管理者ならば商品マスタページへ
		if (managerFLG == 1) {
			response.sendRedirect("item");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// DAOへのデータ梱包用DTO
		InsertItemDTO insertItemDTO = new InsertItemDTO();

		// Postリクエストを得る
		request.setCharacterEncoding("UTF-8");
		for (Part p : request.getParts()) {
			if ("file1".equals(p.getName())) {
				System.out.println(p.getInputStream());
				insertItemDTO.setImage(p.getInputStream());
			} else if ("category".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				insertItemDTO.setCategoryId(br.readLine());
			} else if ("genre".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				insertItemDTO.setGenreId(br.readLine());
			} else if ("status".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				insertItemDTO.setNewAndOldId(br.readLine());
			} else if ("title".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
				insertItemDTO.setTitle(br.readLine());
			} else if ("artist".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
				insertItemDTO.setArtist(br.readLine());
			} else if ("price".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				insertItemDTO.setPrice(br.readLine());
			} else if ("other".equals(p.getName())) {
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
				insertItemDTO.setOther(br.readLine());
			}
		}

		// // Postリクエストを得る
		// request.setCharacterEncoding("UTF-8");
		// String inputCategoryId = request.getParameter("category");
		// String inputGenreId = request.getParameter("genre");
		// String inputStatusId = request.getParameter("status");
		// String inputTitle = request.getParameter("title");
		// String inputArtist = request.getParameter("artist");
		// String inputPrice = request.getParameter("price");
		// String inputOther = request.getParameter("other");

		// DTOに入力値を設定
		// insertItemDTO.setCategoryId(inputCategoryId);
		// insertItemDTO.setGenreId(inputGenreId);
		// insertItemDTO.setNewAndOldId(inputStatusId);
		// insertItemDTO.setTitle(inputTitle);
		// insertItemDTO.setArtist(inputArtist);
		// insertItemDTO.setPrice(inputPrice);
		// insertItemDTO.setOther(inputOther);

		try (Connection conn = DataSourceManager.getConnection()) {
			// 挿入を実行
			ItemDAO itemDAO = new ItemDAO(conn);
			int result = itemDAO.insertNewItem(insertItemDTO);

			// 失敗
			if (result == 0) {
				session.setAttribute("insert_item_message", "商品の追加に失敗しました。");
			}

			// 成功
			if (result == 1) {
				session.setAttribute("insert_item_message", "商品を追加しました。");
			}

			// 遷移
			response.sendRedirect("item");
			// request.getRequestDispatcher("basicProductInformation").forward(request,
			// response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("newItem");
			return;
		}
	}

}
