package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.CategoryDAO;
import jp.mnt.dao.GenreDAO;
import jp.mnt.dao.ItemDAO;
import jp.mnt.dao.NewAndOldDAO;
import jp.mnt.dto.CategoryDTO;
import jp.mnt.dto.GenreDTO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.dto.NewAndOldDTO;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/itemDetail")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// Getリクエスト
		String inputItemId = request.getParameter("itemId");

		// TODO IDチェック

		// ItemIDの情報をDBから取得
		try (Connection conn = DataSourceManager.getConnection()) {

			// 商品情報
			ItemDAO itemDAO = new ItemDAO(conn);
			ArrayList<ItemDTO> itemList = itemDAO.selectItemId(Integer.parseInt(inputItemId));

			// category
			CategoryDAO categoryDAO = new CategoryDAO(conn);
			ArrayList<CategoryDTO> categoryList = categoryDAO.selectAll();

			// GENRE
			GenreDAO genreDAO = new GenreDAO(conn);
			ArrayList<GenreDTO> genreList = genreDAO.selectAll();

			// 新旧区分
			NewAndOldDAO newAndOldDAO = new NewAndOldDAO(conn);
			ArrayList<NewAndOldDTO> newAndOldList = newAndOldDAO.selectAll();

			// リクエストスコープにリストを格納
			request.setAttribute("item_list", itemList);
			request.setAttribute("category_list", categoryList);
			request.setAttribute("genre_list", genreList);
			request.setAttribute("new_and_old_list", newAndOldList);

			// 遷移
			request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp").forward(request, response);
		} catch (SQLException | NamingException e) {
			response.sendRedirect("item");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		String detail = request.getParameter("detail");
		String add = request.getParameter("add");

		if (detail == null) {
			request.setAttribute("add", add);
			request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp").forward(request, response);
			return;
		}

	}

}
