package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.CategoryDAO;
import jp.mnt.dao.GenreDAO;
import jp.mnt.dao.NewAndOldDAO;
import jp.mnt.dto.CategoryDTO;
import jp.mnt.dto.GenreDTO;
import jp.mnt.dto.NewAndOldDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/newItem")
public class NewItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 新規登録に使うであろう情報をDBから取得
		try (Connection conn = DataSourceManager.getConnection()) {

			// カテゴリの取得
			CategoryDAO categoryDAO = new CategoryDAO(conn);
			ArrayList<CategoryDTO> categoryList = categoryDAO.selectAll();

			// ジャンルの取得
			GenreDAO genreDAO = new GenreDAO(conn);
			ArrayList<GenreDTO> genreList = genreDAO.selectAll();

			// 新旧区分の取得
			NewAndOldDAO newAndOldDAO = new NewAndOldDAO(conn);
			ArrayList<NewAndOldDTO> newAndOldList = newAndOldDAO.selectAll();

			request.setAttribute("category_list", categoryList);
			request.setAttribute("genre_list", genreList);
			request.setAttribute("new_and_old_list", newAndOldList);

			// JSPに遷移
			request.getRequestDispatcher("/WEB-INF/jsp/newItemView.jsp").forward(request, response);

		} catch (SQLException |NamingException | NullPointerException | SystemError e) {
			response.sendRedirect("rental");
			return;
		}
	}

}
