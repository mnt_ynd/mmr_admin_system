package jp.mnt.item;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.CategoryDAO;
import jp.mnt.dao.GenreDAO;
import jp.mnt.dao.NewAndOldDAO;
import jp.mnt.dto.CategoryDTO;
import jp.mnt.dto.GenreDTO;
import jp.mnt.dto.NewAndOldDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/item")
public class ItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		HttpSession session = request.getSession(false);

		if (session == null) {
			response.sendRedirect("login");
			return;
		}
		// ログインしていない場合はログインへ遷移
		if (session.getAttribute("staff_id") == null && session.getAttribute("manager_flg") == null) {
			response.sendRedirect("login");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			ArrayList<CategoryDTO> categoryList;
			ArrayList<GenreDTO> genreList;
			ArrayList<NewAndOldDTO> newAndOldList;

			// DBから取得
			CategoryDAO categoryDao = new CategoryDAO(conn);
			categoryList = categoryDao.selectAll();
			GenreDAO genreDao = new GenreDAO(conn);
			genreList = genreDao.selectAll();
			NewAndOldDAO newAndOldDAO = new NewAndOldDAO(conn);
			newAndOldList = newAndOldDAO.selectAll();

			session.setAttribute("categoryList", categoryList);
			session.setAttribute("genreList", genreList);
			session.setAttribute("new_and_old_list", newAndOldList);

			request.getRequestDispatcher("/WEB-INF/jsp/basicProductInformation.jsp").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("item");
			return;
		}

	}

}
