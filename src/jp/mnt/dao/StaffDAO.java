package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.StaffDTO;
import jp.mnt.exception.SystemError;

/**
 * スタッフマスタ用DAO
 *
 * @author ソフトブレーン富岡
 *
 */
public class StaffDAO {
	Connection conn = null;

	public StaffDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	/**
	 * 従業員の全件取得
	 *
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<StaffDTO> selectAll() {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STAFF_ID");
		sb.append("        ,LAST_NAME");
		sb.append("        ,FIRST_NAME");
		sb.append("        ,MANAGER_FLG");
		sb.append("    FROM");
		sb.append("        staff");

		// リストの作成
		ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();
		// SQL文実行
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				StaffDTO empRowData = new StaffDTO();
				empRowData.setStaffId(rs.getInt("STAFF_ID"));
				empRowData.setFirstName(rs.getString("FIRST_NAME"));
				empRowData.setLastName(rs.getString("LAST_NAME"));
				empRowData.setManagerFlg(rs.getInt("MANAGER_FLG"));
				list.add(empRowData);
			}

			return list;
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 従業員の全件取得
	 *
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<StaffDTO> selectAllStaffExceptDeleteStaff() {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STAFF_ID");
		sb.append("        ,LAST_NAME");
		sb.append("        ,FIRST_NAME");
		sb.append("        ,MANAGER_FLG");
		sb.append("    FROM");
		sb.append("        staff");
		sb.append("    WHERE");
		sb.append("        DELETE_FLG = 0");

		// リストの作成
		ArrayList<StaffDTO> list = new ArrayList<StaffDTO>();
		// SQL文実行
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				StaffDTO empRowData = new StaffDTO();
				empRowData.setStaffId(rs.getInt("STAFF_ID"));
				empRowData.setFirstName(rs.getString("FIRST_NAME"));
				empRowData.setLastName(rs.getString("LAST_NAME"));
				empRowData.setManagerFlg(rs.getInt("MANAGER_FLG"));
				list.add(empRowData);
			}
			return list;
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 指定のID、パスワードでログインできるかを判定し、従業員の情報を返す
	 *
	 * @author ソフトブレーン株式会社 富岡
	 * @param メールアドレス
	 * @param パスワード
	 * @return ログイン可能ならtrue
	 * @throws SQLException
	 */
	public ArrayList<StaffDTO> selectByIdAndPassword(String mail, String password) {

		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        STAFF_ID");
		sb.append("        ,LAST_NAME");
		sb.append("        ,FIRST_NAME");
		sb.append("        ,MANAGER_FLG");
		sb.append("    FROM");
		sb.append("        STAFF");
		sb.append("    WHERE");
		sb.append("        MAIL = ?");
		sb.append("        AND PASSWORD = PASSWORD(?)");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?部分に値をセットして検索
			ps.setString(1, mail);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			// データ返送用DTO
			ArrayList<StaffDTO> list = new ArrayList<>();

			// 結果が存在しているならtrue
			if (rs.next()) {
				StaffDTO staffRowData = new StaffDTO();
				staffRowData.setStaffId(rs.getInt("STAFF_ID"));
				staffRowData.setFirstName(rs.getString("FIRST_NAME"));
				staffRowData.setLastName(rs.getString("LAST_NAME"));
				staffRowData.setManagerFlg(rs.getInt("MANAGER_FLG"));
				list.add(staffRowData);
			}
			return list;
		} catch (Exception e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 従業員の新規追加
	 *
	 * @param insertData
	 * @return
	 */
	public int insertStaff(String[] insertData) {

		// SQL文
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        staff(");
		sb.append("            STAFF_ID");
		sb.append("            ,LAST_NAME");
		sb.append("            ,FIRST_NAME");
		sb.append("            ,PASSWORD");
		sb.append("            ,MANAGER_FLG");
		sb.append("            ,DELETE_FLG");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            null");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,PASSWORD(?)");
		sb.append("            ,?");
		sb.append("            ,0");
		sb.append("        );");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?部分に値をセットして検索
			ps.setString(1, insertData[0]);
			ps.setString(2, insertData[1]);
			ps.setString(3, "staff");
			ps.setString(4, insertData[2]);

			return ps.executeUpdate();

		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 従業員の編集
	 *
	 * @param insertData
	 * @return
	 */
	public int updateStaff(String[] repairData) {

		// SQL文
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        LAST_NAME = ?");
		sb.append("        ,FIRST_NAME = ?");
		sb.append("        ,MANAGER_FLG = ?");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// ?部分に値をセットして検索
			ps.setString(1, repairData[1]);
			ps.setString(2, repairData[2]);
			ps.setString(3, repairData[3]);
			ps.setInt(4, Integer.parseInt(repairData[0]));

			return ps.executeUpdate();

		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 従業員の論理削除
	 *
	 * @param deleteId
	 * @return
	 */
	public int deteleStaff(String deleteId) {

		// SQL生成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        DELETE_FLG = 1");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プリペアードステートメントに値を設定
			ps.setString(1, deleteId);

			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * 従業員のパスワードリセット
	 *
	 * @param staffId
	 * @return
	 */
	public int resetStaffPassword(String staffId) {

		// SQL文生成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        staff");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD('staff')");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// 値を設定
			ps.setInt(1, Integer.parseInt(staffId));

			return ps.executeUpdate();

		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}
}
