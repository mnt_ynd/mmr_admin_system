package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.NewAndOldDTO;
import jp.mnt.exception.SystemError;

public class NewAndOldDAO {
	Connection conn = null;

	public NewAndOldDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<NewAndOldDTO> selectAll() {

		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        NEW_AND_OLD_ID");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        new_and_old;");

		// リストの作成
		ArrayList<NewAndOldDTO> list = new ArrayList<NewAndOldDTO>();

		// SQL文実行
		try (PreparedStatement pStatement = conn.prepareStatement(sb.toString())) {
			ResultSet rs = pStatement.executeQuery();

			// 結果をリストに格納
			while (rs.next()) {
				NewAndOldDTO rowData = new NewAndOldDTO();
				rowData.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				rowData.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(rowData);
			}

			return list;
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}
}
