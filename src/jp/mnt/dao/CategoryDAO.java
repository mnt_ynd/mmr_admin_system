package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.CategoryDTO;
import jp.mnt.exception.SystemError;

public class CategoryDAO {
	Connection conn = null;

	public CategoryDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<CategoryDTO> selectAll() {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        CATEGORY_ID");
		sb.append("        ,CATEGORY_NAME");
		sb.append("    FROM");
		sb.append("       CATEGORY;");

		// リストの作成
		ArrayList<CategoryDTO> list = new ArrayList<CategoryDTO>();
		// SQL文実行
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				CategoryDTO empRowData = new CategoryDTO();
				empRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				empRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(empRowData);
			}

			return list;

		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}
}
