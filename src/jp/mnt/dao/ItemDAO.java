package jp.mnt.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.naming.NamingException;

import jp.mnt.dto.InsertItemDTO;
import jp.mnt.dto.ItemDTO;
import jp.mnt.exception.SystemError;

public class ItemDAO {
	Connection conn = null;

	public ItemDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	/**
	 * 検索
	 *
	 * @param searchString
	 * @return
	 * @throws SQLException
	 * @throws NullPointerException
	 * @throws NamingException
	 */
	public ArrayList<ItemDTO> selectText(String searchString)
			throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");

		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

		try {
			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ItemDTO itemRowData = new ItemDTO();
				itemRowData.setItemId(rs.getInt("ITEM_ID"));
				itemRowData.setItemName(rs.getString("ITEM_NAME"));
				itemRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				itemRowData.setGenreId(rs.getInt("GENRE_ID"));
				itemRowData.setRecommendedFlg(rs.getInt("RECOMMENDED_FLG"));
				itemRowData.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				// itemRowData.setImage(rs.getBytes("IMAGE"));
				itemRowData.setArtist(rs.getString("ARTIST"));
				itemRowData.setPrice(rs.getInt("PRICE"));
				itemRowData.setRemarks(rs.getString("REMARKS"));
				itemRowData.setMaxIdentifiction(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
				itemRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				itemRowData.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(itemRowData);
			}

			return list;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}

	}

	public int selectTextCount(String searchString) throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) AS COUNT");
		sb.append("        ,ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");

		int cnt = 0;
		try {

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));

			ResultSet rs = ps.executeQuery();

			rs.next();

			cnt = rs.getInt(1);

			return cnt;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

	public ArrayList<ItemDTO> selectTextAndCategory(String searchString, String category)
			throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");
		sb.append("        AND ITEM.CATEGORY_ID = ?");

		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

		try {

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));
			ps.setInt(2, Integer.parseInt(category));

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				ItemDTO itemRowData = new ItemDTO();
				itemRowData.setItemId(rs.getInt("ITEM_ID"));
				itemRowData.setItemName(rs.getString("ITEM_NAME"));
				itemRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				itemRowData.setGenreId(rs.getInt("GENRE_ID"));
				itemRowData.setRecommendedFlg(rs.getInt("RECOMMENDED_FLG"));
				itemRowData.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				// itemRowData.setImage(rs.getBytes("IMAGE"));
				itemRowData.setArtist(rs.getString("ARTIST"));
				itemRowData.setPrice(rs.getInt("PRICE"));
				itemRowData.setRemarks(rs.getString("REMARKS"));
				itemRowData.setMaxIdentifiction(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
				itemRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				itemRowData.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(itemRowData);
			}

			return list;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

	public int selectTextCategoryCount(String searchString, String category)
			throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) As COUNT");
		sb.append("        ,ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");
		sb.append("        AND ITEM.CATEGORY_ID = ?");

		int cnt = 0;
		try {

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));
			ps.setInt(2, Integer.parseInt(category));

			ResultSet rs = ps.executeQuery();

			rs.next();
			cnt = rs.getInt(1);

			return cnt;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

	public ArrayList<ItemDTO> selectTextAndCategoryAndGenre(String searchString, String category, String genre)
			throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");
		sb.append("        AND ITEM.CATEGORY_ID = ?");
		sb.append("        AND GENRE_ID = ?");

		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

		try {

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));
			ps.setInt(2, Integer.parseInt(category));
			ps.setInt(3, Integer.parseInt(genre));

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				ItemDTO itemRowData = new ItemDTO();
				itemRowData.setItemId(rs.getInt("ITEM_ID"));
				itemRowData.setItemName(rs.getString("ITEM_NAME"));
				itemRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				itemRowData.setGenreId(rs.getInt("GENRE_ID"));
				itemRowData.setRecommendedFlg(rs.getInt("RECOMMENDED_FLG"));
				itemRowData.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				// itemRowData.setImage(rs.getBytes("IMAGE"));
				itemRowData.setArtist(rs.getString("ARTIST"));
				itemRowData.setPrice(rs.getInt("PRICE"));
				itemRowData.setRemarks(rs.getString("REMARKS"));
				itemRowData.setMaxIdentifiction(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
				itemRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				itemRowData.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(itemRowData);
			}

			return list;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

	public int selectTextCategoryAndGenreCount(String searchString, String category, String genre)
			throws SQLException, NullPointerException, NamingException {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        COUNT(*) As COUNT");
		sb.append("        ,ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_NAME like ?");
		sb.append("        AND ITEM.CATEGORY_ID = ?");
		sb.append("        AND GENRE_ID = ?");

		int cnt = 0;
		try {

			PreparedStatement ps = conn.prepareStatement(sb.toString());

			searchString = Normalizer.normalize(searchString, Normalizer.Form.NFKC);

			ps.setString(1, String.format("%1$s%2$s%1$s", "%", searchString));
			ps.setInt(2, Integer.parseInt(category));
			ps.setInt(3, Integer.parseInt(genre));

			ResultSet rs = ps.executeQuery();

			rs.next();
			cnt = rs.getInt(1);

			return cnt;
		} catch (SQLException e) {
			throw e;
		} catch (NullPointerException e) {
			throw e;
		}
	}

	public ArrayList<ItemDTO> selectItemId(int itemId) {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        ITEM_ID");
		sb.append("        ,ITEM_NAME");
		sb.append("        ,ITEM.CATEGORY_ID");
		sb.append("        ,GENRE_ID");
		sb.append("        ,RECOMMENDED_FLG");
		sb.append("        ,ITEM.NEW_AND_OLD_ID");
		// sb.append(" ,IMAGE");
		sb.append("        ,ARTIST");
		sb.append("        ,PRICE");
		sb.append("        ,REMARKS");
		sb.append("        ,MAX_IDENTIFICATION_NUMBER");
		sb.append("        ,CATEGORY_NAME");
		sb.append("        ,NEW_AND_OLD_NAME");
		sb.append("    FROM");
		sb.append("        ITEM");
		sb.append("        left join CATEGORY on ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("        left join NEW_AND_OLD on ITEM.NEW_AND_OLD_ID = NEW_AND_OLD.NEW_AND_OLD_ID");
		sb.append("    WHERE");
		sb.append("        ITEM_ID = ?");

		ArrayList<ItemDTO> list = new ArrayList<ItemDTO>();

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ps.setInt(1, itemId);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				ItemDTO itemRowData = new ItemDTO();
				itemRowData.setItemId(rs.getInt("ITEM_ID"));
				itemRowData.setItemName(rs.getString("ITEM_NAME"));
				itemRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				itemRowData.setGenreId(rs.getInt("GENRE_ID"));
				itemRowData.setRecommendedFlg(rs.getInt("RECOMMENDED_FLG"));
				itemRowData.setNewAndOldId(rs.getString("NEW_AND_OLD_ID"));
				// itemRowData.setImage(rs.getBytes("IMAGE"));
				itemRowData.setArtist(rs.getString("ARTIST"));
				itemRowData.setPrice(rs.getInt("PRICE"));
				itemRowData.setRemarks(rs.getString("REMARKS"));
				itemRowData.setMaxIdentifiction(rs.getInt("MAX_IDENTIFICATION_NUMBER"));
				itemRowData.setCategoryName(rs.getString("CATEGORY_NAME"));
				itemRowData.setNewAndOldName(rs.getString("NEW_AND_OLD_NAME"));
				list.add(itemRowData);
			}

			return list;
		} catch (SQLException | NullPointerException e) {
			throw new SystemError(e);
		}
	}

	/**
	 * IDを指定し、画像を復元
	 *
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public BufferedImage selectImageById(int itemId) throws Exception {
		try {

			// SQL文を作成する
			StringBuffer sbSQL = new StringBuffer();
			sbSQL.append("   SELECT");
			sbSQL.append("          IMAGE");
			sbSQL.append("     FROM");
			sbSQL.append("          ITEM");
			sbSQL.append("    WHERE");
			sbSQL.append("          ITEM_ID = ?");

			// 実行する
			PreparedStatement ps = conn.prepareStatement(sbSQL.toString());
			ps.setInt(1, itemId);
			ResultSet rs = ps.executeQuery();

			// 結果から画像データを取得し、返す。
			if (rs.next()) {
				InputStream is = rs.getBinaryStream("IMAGE");
				BufferedInputStream bis = new BufferedInputStream(is);
				return ImageIO.read(bis);
			}

		} catch (IOException | SQLException e) {
			throw new SystemError(e);
		}
		return null;
	}

	/**
	 * 新規商品追加
	 *
	 * @param dto
	 * @return
	 */
	public int insertNewItem(InsertItemDTO dto) {

		// SQL文作成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        item");
		sb.append("      (");
		sb.append("          ITEM_ID");
		sb.append("          ,ITEM_NAME");
		sb.append("          ,CATEGORY_ID");
		sb.append("          ,GENRE_ID");
		sb.append("          ,RECOMMENDED_FLG");
		sb.append("          ,NEW_AND_OLD_ID");
		sb.append("          ,IMAGE");
		sb.append("          ,ARTIST");
		sb.append("          ,PRICE");
		sb.append("          ,REMARKS");
		sb.append("          ,MAX_IDENTIFICATION_NUMBER");
		sb.append("          ,CREATE_DATE");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            null");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,null");
		sb.append("            ,?");
		// sb.append(" ,null");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,null");
		sb.append("            ,current_timestamp()");
		sb.append("        );");

		try {
			PreparedStatement ps = conn.prepareStatement(sb.toString());
			// ?部分に値をセットして検索
			// ps.setString(1, dto.getTitle());
			// ps.setInt(2, Integer.parseInt(dto.getCategoryId()));
			// ps.setInt(3, Integer.parseInt(dto.getGenreId()));
			// ps.setInt(4, Integer.parseInt(dto.getNewAndOldId()));
			// ps.setString(5, dto.getArtist());
			// ps.setInt(6, Integer.parseInt(dto.getPrice()));
			// ps.setString(7, dto.getOther());
			ps.setString(1, dto.getTitle());
			ps.setInt(2, Integer.parseInt(dto.getCategoryId()));
			ps.setInt(3, Integer.parseInt(dto.getGenreId()));
			ps.setInt(4, Integer.parseInt(dto.getNewAndOldId()));
			ps.setBinaryStream(5, dto.getImage());
			ps.setString(6, dto.getArtist());
			ps.setInt(7, Integer.parseInt(dto.getPrice()));
			ps.setString(8, dto.getOther());

			return ps.executeUpdate();

		} catch (SQLException | NumberFormatException | NullPointerException e) {
			throw new SystemError(e);
		}
	}
}
