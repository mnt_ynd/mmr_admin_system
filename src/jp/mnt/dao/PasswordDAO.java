package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.mnt.exception.SystemError;

public class PasswordDAO {
	Connection conn = null;

	public PasswordDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public void updatePassword(Integer id, String pass) {

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        STAFF");
		sb.append("    SET");
		sb.append("        PASSWORD = PASSWORD(?)");
		sb.append("    WHERE");
		sb.append("        STAFF_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setString(1, pass);
			ps.setInt(2, id);

			ps.executeUpdate();

		} catch (SQLException | NullPointerException e) {
			throw new SystemError(e);
		}
	}

}
