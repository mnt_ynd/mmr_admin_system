package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.mnt.dto.GenreDTO;
import jp.mnt.exception.SystemError;

public class GenreDAO {
	Connection conn = null;

	public GenreDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public ArrayList<GenreDTO> selectAll() {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        GENRE_ID");
		sb.append("        ,CATEGORY_ID");
		sb.append("        ,GENRE_NAME");
		sb.append("    FROM");
		sb.append("        genre;");

		// リストの作成
		ArrayList<GenreDTO> list = new ArrayList<GenreDTO>();
		// SQL文実行
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			ResultSet rs = ps.executeQuery();

			// SQLの結果を取得し、リストに詰める
			while (rs.next()) {
				GenreDTO empRowData = new GenreDTO();
				empRowData.setGenreId(rs.getInt("GENRE_ID"));
				empRowData.setCategoryId(rs.getInt("CATEGORY_ID"));
				empRowData.setGenreName(rs.getString("GENRE_NAME"));
				list.add(empRowData);
			}

			return list;
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}
}
