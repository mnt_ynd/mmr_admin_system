package jp.mnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.mnt.dto.UploadImageDTO;
import jp.mnt.exception.SystemError;

public class UploadImageDAO {
	Connection conn = null;

	public UploadImageDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public int upload(UploadImageDTO dto) {

		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("        item");
		sb.append("    SET");
		sb.append("        IMAGE = ?");
		sb.append("  WHERE");
		sb.append("        item_id = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			ps.setString(1, dto.getName());
			ps.setBinaryStream(2, dto.getImage());
			ps.setInt(3, dto.getId());

			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SystemError(e);
		}
	}
}
