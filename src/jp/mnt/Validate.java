package jp.mnt;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Validate {

	/**
	 * 名前が20文字以内であるかどうかの判定
	 *
	 * @param name
	 * @return
	 */
	public static boolean isName(String name) {
		try {
			if (name.isEmpty()) {
				return false;
			}
			if (name.length() >= 1 && name.length() <= 20) {
				return true;
			}
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}

	/**
	 * managerIDが正しいかどうかを判定
	 *
	 * @param managerFLG
	 * @return
	 */
	public static boolean isManagerFLG(String managerFLG) {
		return true;
	}

	/**
	 * 数値かどうかの判定
	 *
	 * @param num
	 * @return
	 */
	public static boolean isNum(String num) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^[0-9]*$");
		java.util.regex.Matcher matcher = pattern.matcher(num);
		return matcher.matches();
	}

	public static boolean isNumNullAble(String num) {
		if (num == null) {
			return true;
		}
		if (num.length() == 0) {
			return true;
		}
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^[0-9]*$");
		java.util.regex.Matcher matcher = pattern.matcher(num);
		return matcher.matches();
	}

	/**
	 * 日付として正しいデータかどうかを判定する
	 *
	 * @param strDate
	 * @return
	 */
	public static boolean isDateNullAble(String strDate) {
		if (strDate == null || strDate.length() == 0) {
			return true;
		}
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate.parse(strDate, dtf);
			return true;
		} catch (DateTimeParseException dtp) {
			return false;
		}
	}
}