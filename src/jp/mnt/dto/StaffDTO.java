package jp.mnt.dto;

//スタッフテーブル
public class StaffDTO {
	private int staffId;
	private String password;
	private int managerFlg;
	private String lastName;
	private String firstName;
	private int deleteFlg;
	
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getManagerFlg() {
		return managerFlg;
	}
	public void setManagerFlg(int managerFlg) {
		this.managerFlg = managerFlg;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getDeleteFlg() {
		return deleteFlg;
	}
	public void setDeleteFlg(int deleteFlg) {
		this.deleteFlg = deleteFlg;
	}
	
	
}
