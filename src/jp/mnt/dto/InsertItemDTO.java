package jp.mnt.dto;

import java.io.InputStream;

public class InsertItemDTO {
	private InputStream image;
	private String categoryId;
	private String genreId;
	private String newAndOldId;
	private String title;
	private String artist;
	private String price;
	private String other;

	public InputStream getImage() {
		return image;
	}

	public void setImage(InputStream image) {
		this.image = image;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getGenreId() {
		return genreId;
	}

	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}

	public String getNewAndOldId() {
		return newAndOldId;
	}

	public void setNewAndOldId(String newAndOldId) {
		this.newAndOldId = newAndOldId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
}
