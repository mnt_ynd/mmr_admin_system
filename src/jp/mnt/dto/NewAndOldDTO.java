package jp.mnt.dto;

//新旧区分IDテーブル
public class NewAndOldDTO {
	private String newAndOldId;
	private String newAndOldName;

	public String getNewAndOldId() {
		return newAndOldId;
	}

	public String setNewAndOldId(String newAndOldId) {
		return this.newAndOldId = newAndOldId;
	}

	public String getNewAndOldName() {
		return newAndOldName;
	}

	public void setNewAndOldName(String newAndOldName) {
		this.newAndOldName = newAndOldName;
	}

}
