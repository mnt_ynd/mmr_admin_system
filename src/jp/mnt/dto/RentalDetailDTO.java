package jp.mnt.dto;

import java.sql.Date;

//レンタル詳細テーブル
public class RentalDetailDTO {

	// レンタル詳細テーブル変数
	private int rentalDetailNumber;
	private int rentalNumber;
	private int itemId;
	private int price;
	private int identificationNumber;

	// レンタルテーブル
	private Date orderDatetime;
	private Date toReturnDatetime;
	private Date returnDatetime;
	private int count;
	private String rentalStaffName;
	private String returnStaffName;
	private String statusName;
	private int statusId;

	// 商品テーブル
	private String categoryName;
	private String genreName;
	private String itemTitle;

	// その他
	private String countContents;

	public int getRentalDetailNumber() {
		return rentalDetailNumber;
	}

	public void setRentalDetailNumber(int rentalDetailNumber) {
		this.rentalDetailNumber = rentalDetailNumber;
	}

	public int getRentalNumber() {
		return rentalNumber;
	}

	public void setRentalNumber(int rentalNumber) {
		this.rentalNumber = rentalNumber;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(int identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public Date getOrderDatetime() {
		return orderDatetime;
	}

	public void setOrderDatetime(Date orderDatetime) {
		this.orderDatetime = orderDatetime;
	}

	public Date getToReturnDatetime() {
		return toReturnDatetime;
	}

	public void setToReturnDatetime(Date toRetunrDatetime) {
		this.toReturnDatetime = toRetunrDatetime;
	}

	public Date getReturnDatetime() {
		return returnDatetime;
	}

	public void setReturnDatetime(Date returnDatetime) {
		this.returnDatetime = returnDatetime;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getRentalStaffName() {
		return rentalStaffName;
	}

	public void setRentalStaffName(String rentalStaffName) {
		this.rentalStaffName = rentalStaffName;
	}

	public String getReturnStaffName() {
		return returnStaffName;
	}

	public void setReturnStaffName(String returnStaffName) {
		this.returnStaffName = returnStaffName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	public String getCountContents() {
		return countContents;
	}

	public void setCountContents(String countContents) {
		this.countContents = countContents;
	}

}
