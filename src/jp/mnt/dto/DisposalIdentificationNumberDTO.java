package jp.mnt.dto;

//廃棄個体識別番号テーブル
public class DisposalIdentificationNumberDTO {
	private int itemId;
	private int identificationNumber;
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(int identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
}
