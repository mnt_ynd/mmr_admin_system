package jp.mnt.analytics;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.AnalyticsDAO;

/**
 * Servlet implementation class analyticsServlet
 */
@WebServlet("/analytics")
public class analyticsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// 管理者でなければレンタル検索ページへ
		if (managerFLG == 0) {
			response.sendRedirect("rental");
			return;
		}

		// 分析機能
		try (Connection conn = DataSourceManager.getConnection()) {

			// DAO
			AnalyticsDAO analyticsDAO = new AnalyticsDAO(conn);

			// 先月の年代別利用者数
			int[] thisYear = analyticsDAO.thisYear();

			// 去年の先月の年代別利用者数
			int[] lastYear = analyticsDAO.lastYear();

			// グラフ用にデータを作成
			int[] data = new int[7];
			for (int i = 0; i < 7; i++) {
				data[i] = thisYear[i] - lastYear[i];
			}

			// リクエストスコープに設定
			request.setAttribute("this_year", thisYear);
			request.setAttribute("last_year", lastYear);
			request.setAttribute("data", data);

			// analyticsへ遷移
			request.getRequestDispatcher("/WEB-INF/jsp/analyticsView.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {
			response.sendRedirect("error.jsp");
		}
	}

}
