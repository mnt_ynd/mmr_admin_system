package jp.mnt.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.dao.RentalDetailDAO;
import jp.mnt.dto.RentalDetailDTO;
import jp.mnt.exception.SystemError;

/**
 * Servlet implementation class RentalDetailServlet
 */
@WebServlet("/rentalDetail")
public class RentalDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession(false);

		// セッションがない
		if (session == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// 未ログイン
		if (session.getAttribute("staff_id") == null || session.getAttribute("manager_flg") == null) {
			// ログインページへ遷移
			response.sendRedirect("login");
			return;
		}

		// ログイン済み
		// Integer staffId = (Integer) session.getAttribute("staff_id");
		// Integer managerFLG = (Integer) session.getAttribute("manager_flg");

		// URLクエリパラメータを取得
		String rentalNumber = request.getParameter("rentalNumber");

		// 入力値が正しくなければエラーページへ
		if (isEmpno(rentalNumber) == false) {
			response.sendRedirect("rental");
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// レンタル詳細を取得
			RentalDetailDAO rentalDetailDao = new RentalDetailDAO(conn);
			ArrayList<RentalDetailDTO> rentalDetailList = rentalDetailDao.selectByRentalNumber(rentalNumber);

			// レンタル内容を取得
			ArrayList<RentalDetailDTO> rentalContentsList = rentalDetailDao.selectContentsByRentalNumber(rentalNumber);

			// リクエストスコープにデータを設定する
			session.setAttribute("rental_Number", rentalNumber);
			request.setAttribute("rental_Detail_List", rentalDetailList);
			request.setAttribute("rental_Contens_List", rentalContentsList);

			// 遷移する
			request.getRequestDispatcher("/WEB-INF/jsp/rentalDetailView.jsp").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("item");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public boolean isEmpno(String rentalNumber) {
		if (rentalNumber == null) {
			return false;
		}
		try {
			Integer.parseInt(rentalNumber);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
