package jp.mnt.rental;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.mnt.DataSourceManager;
import jp.mnt.Validate;
import jp.mnt.dao.RentalDetailDAO;
import jp.mnt.dao.StaffDAO;
import jp.mnt.dao.StatusDAO;
import jp.mnt.dto.RentalDetailDTO;
import jp.mnt.dto.StaffDTO;
import jp.mnt.dto.StatusDTO;
import jp.mnt.exception.SystemError;

/**
 * レンタル情報の検索
 *
 * @author ソフトブレーン富岡
 *
 */
@WebServlet("/rental")
public class RentalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションの取得
		HttpSession session = request.getSession(false);

		if (session == null) {
			response.sendRedirect("login");
			return;
		}
		// ログインしていない場合はログインへ遷移
		if (session.getAttribute("staff_id") == null && session.getAttribute("manager_flg") == null) {
			response.sendRedirect("login");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// 従業員一覧を取得(検索用セレクトボックス)
			StaffDAO staffDao = new StaffDAO(conn);
			ArrayList<StaffDTO> staffList = staffDao.selectAll();

			// ステータステーブルの値を取得(検索用セレクトボックス)
			StatusDAO statudDao = new StatusDAO(conn);
			ArrayList<StatusDTO> statusList = statudDao.selectAll();

			// レンタル詳細情報を検索
			RentalDetailDAO rentalDetailDao = new RentalDetailDAO(conn);
			ArrayList<RentalDetailDTO> rentalDetailList = null;

			// 検索ボタンのGetリクエスト
			String orderDate = request.getParameter("orderDate");
			String toReturnDate = request.getParameter("toReturnDate");
			String returnDate = request.getParameter("returnDate");
			String rentalStaffId = validetaNum(request.getParameter("rentalStaffId"));
			String returnStaffId = validetaNum(request.getParameter("returnStaffId"));
			String statusId = validetaNum(request.getParameter("statusId"));
			String[] inputStr = { orderDate, toReturnDate, returnDate, rentalStaffId, returnStaffId, statusId };

			// 入力エラー文用リスト
			ArrayList<String> errorMessage = new ArrayList<>();

			if (!Validate.isDateNullAble(orderDate)) {
				errorMessage.add("無効な受付日が入力されました");
			}
			if (!Validate.isDateNullAble(toReturnDate)) {
				errorMessage.add("無効な返却予定日が入力されました。");
			}
			if (!Validate.isDateNullAble(returnDate)) {
				errorMessage.add("無効な返却日が入力されました。");
			}
			if (!Validate.isNumNullAble(request.getParameter("rentalStaffId"))) {
				errorMessage.add("無効な貸出担当者が選択されました。");
			}
			if (!Validate.isNumNullAble(request.getParameter("returnStaffId"))) {
				errorMessage.add("無効な返却担当者が選択されました。");
			}
			if (!Validate.isNumNullAble(request.getParameter("statusId"))) {
				errorMessage.add("無効なステータスが選択されました。");
			}

			// 入力エラーがあれば戻す
			if (errorMessage.size() >= 1) {
				request.setAttribute("input_str", inputStr);
				request.setAttribute("staff_list", staffList);
				request.setAttribute("status_list", statusList);
				request.setAttribute("rental_list", rentalDetailList);
				request.setAttribute("error_message", errorMessage);
				request.getRequestDispatcher("/WEB-INF/jsp/rentalView.jsp").forward(request, response);
				return;
			}

			int countSearch = 0;
			if (inputStr[0] == null) {
				// 初回アクセス時には全件検索を行い、レコード数を返す
				countSearch = rentalDetailDao.selectCountContentsBySearch("", "", "", "", "", "");
			} else {
				// 該当するレンタル情報の個数を検索
				countSearch = rentalDetailDao.selectCountContentsBySearch(orderDate, toReturnDate, returnDate,
						rentalStaffId, returnStaffId, statusId);
			}

			// ページング数を格納
			if (countSearch == 0) {
			} else if (countSearch % 10 == 0) {
				request.setAttribute("page", countSearch / 10);
			} else {
				request.setAttribute("page", countSearch / 10 + 1);
			}

			// 現在のページを取得
			String currentPage = request.getParameter("page");
			int pageNumber = 1;
			if (currentPage == null) {
				pageNumber = 1;
			} else if (validetaNum(currentPage).length() >= 1) {
				pageNumber = Integer.parseInt(currentPage);
			}

			// 表示するレンタル概要の検索
			if (inputStr[0] == null) {
				rentalDetailList = rentalDetailDao.selectBySearch("", "", "", "", "", "", pageNumber);
			} else {
				rentalDetailList = rentalDetailDao.selectBySearch(orderDate, toReturnDate, returnDate, rentalStaffId,
						returnStaffId, statusId, pageNumber);
			}

			// リクエストスコープにデータを設定する
			request.setAttribute("input_str", inputStr);
			request.setAttribute("staff_list", staffList);
			request.setAttribute("status_list", statusList);
			request.setAttribute("rental_list", rentalDetailList);
			request.setAttribute("current_page", pageNumber);
			request.setAttribute("countItem", countSearch);

			// rentalサーブレットへ
			request.getRequestDispatcher("/WEB-INF/jsp/rentalView.jsp").forward(request, response);

		} catch (SQLException | NamingException | SystemError e) {
			response.sendRedirect("rental");
			return;
		}
	}

	public String validetaNum(String num) {
		try {
			Integer.parseInt(num);
			return num;
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
