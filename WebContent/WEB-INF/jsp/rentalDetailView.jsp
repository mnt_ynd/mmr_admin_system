<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>レンタル一覧 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<jsp:include page="menu.jsp" />
	<!-- 詳細 -->
	<div class="container">
		<h2>詳細画面</h2>
		<div class="row">
			<table class="table table-hover">
				<thead>
					<tr class="table-active">
						<th>受付番号</th>
						<th>受付時間</th>
						<th>返却予定日</th>
						<th>返却日</th>
						<th>数量</th>
						<th>貸出担当</th>
						<th>返却担当</th>
						<th>ステータス</th>
					</tr>
				</thead>
				<tbody>

					<c:choose>
						<c:when test="${requestScope.rental_Detail_List.size() == 0 }">
							<tr>
								<td scope="row"><c:out value="${sessionScope.rental_Number}" /></td>
								<td>この受付番号のデータはありません。</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="dto" items="${requestScope.rental_Detail_List}">
								<tr>
									<th scope="row"><c:out value="${dto.rentalNumber}" /></th>
									<td><c:out value="${dto.orderDatetime}" /></td>
									<td><c:out value="${dto.toReturnDatetime}" /></td>
									<td><c:out value="${dto.returnDatetime}" /></td>
									<td><c:out value="${dto.count}" /></td>
									<td><c:out value="${dto.rentalStaffName}" /></td>
									<td><c:out value="${dto.returnStaffName}" /></td>
									<td><c:out value="${dto.statusName}" /></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>

				</tbody>
			</table>
		</div>
	</div>
	<!-- 詳細終わり -->

	<!-- 対象商品 -->
	<div class="container">
		<h2>対象商品</h2>
		<form action="doRentalAndReturn" method="post">
			<div class="row">
				<table class="table table-hover">
					<thead>
						<tr class="table-active">
							<th>カテゴリ</th>
							<th>ジャンル</th>
							<th>タイトル</th>
							<th>個体識別番号</th>
						</tr>
					</thead>
					<tbody>

						<c:choose>
							<c:when test="${requestScope.rental_Contens_List.size() == 0 }">
								<tr>
									<td>データはありません。</td>
								</tr>
							</c:when>
							<c:otherwise>
								<!-- 貸出 -->
								<c:if test="${requestScope.rental_Detail_List[0].statusId == 1}">
									<c:forEach var="dto" varStatus="st" items="${requestScope.rental_Contens_List}">
										<tr>
											<th scope="row"><c:out value="${dto.categoryName}" /></th>
											<td><c:out value="${dto.genreName}" /></td>
											<td><c:out value="${dto.itemTitle}" /></td>
											<td><input type="number" name="<c:out value="item_${dto.rentalDetailNumber}"/>"
												max="999" min="001" required /></td>
										</tr>
									</c:forEach>
								</c:if>
								<!-- 返却 -->
								<c:if test="${requestScope.rental_Detail_List[0].statusId >= 2}">
									<c:forEach var="dto" items="${requestScope.rental_Contens_List}">
										<input type="hidden" value="dto" name="" />
										<tr>
											<th scope="row"><c:out value="${dto.categoryName}" /></th>
											<td><c:out value="${dto.genreName}" /></td>
											<td><c:out value="${dto.itemTitle}" /></td>
											<td><c:out value="${dto.identificationNumber}" /></td>
										</tr>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<input type="hidden" value="${requestScope.rental_Detail_List[0].count}" name="contentsNum" />
			<!-- 貸出処理 -->
			<c:if test="${requestScope.rental_Detail_List[0].statusId == 1}">
				<input type="hidden" value="rental" name="whatProcess" />
				<button type="submit" class="btn btn-info">貸出</button>
			</c:if>
			<c:if
				test="${requestScope.rental_Detail_List[0].statusId == 2 || requestScope.rental_Detail_List[0].statusId == 4 }">
				<input type="hidden" value="return" name="whatProcess" />
				<button type="submit" class="btn btn-success">返却</button>
			</c:if>
		</form>
	</div>
	<!-- 対象商品終わり -->

	<jsp:include page="footer.jsp" />
</body>
</html>