<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>従業員マスタメンテナンス -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- 修正のボタン操作 -->
<script type="text/javascript">
	$(document).ready(
			function() {
				// 初回読み込み時
				// 入力欄を非表示
				$('.definitionBtn').hide();
				$('.cancelBtn').hide();
				$('.staffId').hide();
				$('.lastName').hide();
				$('.firstName').hide();
				$('.managerFLG').hide();

				// 修正をクリック
				$('.repairBtn').click(
						function() {
							// 文字を非表示にし、inputタグを表示
							$(this).parent().parent().children().find(
									'.repairBtn').hide()
							$(this).parent().parent().children().find(
									'.resetBtn').hide();
							$(this).parent().parent().children().find(
									'.pStaffId').hide();
							$(this).parent().parent().children().find(
									'.pLastName').hide();
							$(this).parent().parent().children().find(
									'.pFirstName').hide();
							$(this).parent().parent().children().find(
									'.pManagerFLG').hide();
							$(this).parent().parent().children().find(
									'.cancelBtn').show();
							$(this).parent().parent().children().find(
									'.definitionBtn').show();
							$(this).parent().parent().children().find(
									'.staffId').show();
							$(this).parent().parent().children().find(
									'.lastName').show();
							$(this).parent().parent().children().find(
									'.firstName').show();
							$(this).parent().parent().children().find(
									'.managerFLG').show();
						});
				// 修正キャンセルをクリック
				$('.cancelBtn').click(
						function() {
							// inputを非表示にし、文字を表示
							$(this).parent().parent().children().find(
									'.cancelBtn').hide();
							$(this).parent().parent().children().find(
									'.definitionBtn').hide();
							$(this).parent().parent().children().find(
									'.staffId').hide();
							$(this).parent().parent().children().find(
									'.lastName').hide();
							$(this).parent().parent().children().find(
									'.firstName').hide();
							$(this).parent().parent().children().find(
									'.managerFLG').hide();
							$(this).parent().parent().children().find(
									'.repairBtn').show();
							$(this).parent().parent().children().find(
									'.resetBtn').show();
							$(this).parent().parent().children().find(
									'.pStaffId').show();
							$(this).parent().parent().children().find(
									'.pLastName').show();
							$(this).parent().parent().children().find(
									'.pFirstName').show();
							$(this).parent().parent().children().find(
									'.pManagerFLG').show();
						});

				// 送信フォームにデータを挿入
				$('.definitionBtn').click(
						function(e) {

							var staffId = ($(this).parent().parent().children(
									'.staff').find("input").val());
							var lastName = ($(this).parent().parent().children(
									'.lname').find("input").val());
							var firstName = ($(this).parent().parent()
									.children('.fname').find("input").val());
							var managerFLG = ($(this).parent().parent()
									.children('.mflg').find("select").val());
							$('[name="sbStaffId"]').val(staffId);
							$('[name="sbLastName"]').val(lastName);
							$('[name="sbFirstName"]').val(firstName);
							$('[name="sbManagerFLG"]').val(managerFLG);
							$('#repairForm').trigger("submit");
						});

				//削除ボタンの有効、無効化
				$('.deleteCheckbox').change(function() {

					var count = $(".deleteCheckbox:checked").length;
					if (count == 0) {
						$('#deleteBtn').prop('disabled', true);
					} else {
						$('#deleteBtn').prop('disabled', false);

					}
				});
			});
</script>
</head>

<body>
	<jsp:include page="menu.jsp" />

	<%-- エラーメッセージがあれば表示する --%>
	<!-- 貸出メッセージ -->
	<c:if test="${!empty sessionScope.insert_staff_message}">
		<div class="container">
			<div style="color: red">
				<c:out value="${sessionScope.insert_staff_message}" />
				<c:remove var="insert_staff_message" scope="session" />
			</div>
		</div>
	</c:if>
	<!-- 従業員情報修正メッセージ -->
	<c:if test="${!empty requestScope.repair_staff_message}">
		<div class="container">
			<div style="color: red">
				<c:out value="${requestScope.repair_staff_message}" />
			</div>
		</div>
	</c:if>
	<!-- 従業員パスワードリセットメッセージ -->
	<c:if test="${!empty sessionScope.reset_staff_message}">
		<div class="container">
			<div style="color: red">
				<c:out value="${sessionScope.reset_staff_message}" />
				<c:remove var="reset_staff_message" scope="session" />
			</div>
		</div>
	</c:if>
	<div class="container">
		<h2>従業員マスタメンテナンス</h2>
		<div class="row">
			<table class="table table-hover col-md-12">
				<!-- Tableカラムタイトル -->
				<thead>
					<tr>
						<th>ID</th>
						<th>姓</th>
						<th>名</th>
						<th>パスワード</th>
						<th>権限</th>
						<th>編集</th>
						<th>削除</th>
					</tr>
				</thead>
				<!-- Table内容 -->
				<tbody>
					<c:forEach var="dto" items="${requestScope.staff_list}">
						<tr id="<c:out value="${dto.staffId}" />">
							<td class="staff">
								<div class="pStaffId">
									<c:out value="${dto.staffId}" />
								</div> <input type="number" class="staffId" name="staffId"
								value="<c:out value="${dto.staffId}" />" min="1" max="9999999" readonly />
							</td>
							<td class="lname">
								<div class="pLastName">
									<c:out value="${dto.lastName}" />
								</div> <input type="text" class="lastName" name="" value="<c:out value="${dto.lastName}" />"
								placeholder="苗字" min="1" max="20">
							</td>
							<td class="fname">
								<div class="pFirstName">
									<c:out value="${dto.firstName}" />
								</div> <input type="text" class="firstName" name="" value="<c:out value="${dto.firstName}" />"
								placeholder="名前" min="1" max="20">
							</td>
							<td><a href="resetEmpPass?staffId=<c:out value="${dto.staffId}" />" id="resetBtn"
								class="btn btn-sm btn-default resetBtn" role="button">リセット</a></td>
							<td class="mflg">
								<div class="pManagerFLG">
									<c:if test="${dto.managerFlg == 0}">
									なし
									</c:if>
									<c:if test="${dto.managerFlg == 1}">
									管理者
									</c:if>
								</div> <select class="managerFLG" name="" readonly>
									<c:choose>
										<c:when test="${dto.managerFlg == 0}">
											<option value="0" selected>なし</option>
											<option value="1">管理者</option>
										</c:when>
										<c:otherwise>
											<option value="0">なし</option>
											<option value="1" selected>管理者</option>
										</c:otherwise>
									</c:choose>
							</select>
							</td>
							<td><button type="button" class="btn btn-sm btn-warning repairBtn">修正</button>
								<button type="submit" class="btn btn-sm btn-success definitionBtn">確定</button>
								<button type="reset" class="btn btn-sm cancelBtn" form="repairForm">キャンセル</button></td>
							<td><input class="deleteCheckbox" type="checkbox" form="delteEmpForm" name="deleteId"
								value="<c:out value="${dto.staffId}" />" /></td>
						</tr>
					</c:forEach>
					<!-- 新規追加 -->
					<c:if test="${!empty requestScope.insert_error_message}">
						<tr>
							<td colspan="7" style="color: red;"><c:forEach var="message"
									items="${requestScope.insert_error_message}">
									<c:out value="${message}" />
								</c:forEach></td>
						</tr>
						<c:remove var="insert_error_message" />
					</c:if>
					<tr>
						<td>従業員追加</td>
						<td><input type="text" name="lastName" value="" placeholder="苗字" min="1" max="20"
							form="insertEmpForm"></td>
						<td><input type="text" name="firstName" value="" placeholder="名前" min="1" max="20"
							form="insertEmpForm"></td>
						<td><input type="text" name="password" value="staff" size="5" form="insertEmpForm"
							readonly /></td>
						<td><select name="managerFLG" form="insertEmpForm">
								<option value="0">なし</option>
								<option value="1">管理者</option>
						</select></td>
						<td><button type="submit" class="btn btn-sm btn-success" form="insertEmpForm">新規追加</button></td>
						<td></td>
					</tr>
					<!-- 削除ボタン-->
					<tr>
						<td colspan="6"></td>
						<td>
							<button id="deleteBtn" class="pull-right btn btn-sm btn-danger" type="submit"
								form="delteEmpForm" disabled>削除</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- 送信用疑似form -->
		<form action="insertEmp" method="post" id="insertEmpForm"></form>
		<form action="deleteEmp" method="post" id="delteEmpForm"></form>

		<!-- 編集送信用form -->
		<form action="repairEmp" method="post" id="repairForm">
			<input type="text" name="sbStaffId" value="" style="display: none" /> <input type="text"
				name="sbLastName" value="" style="display: none" /> <input type="text" name="sbFirstName"
				value="" style="display: none" /> <input type="text" name="sbManagerFLG" value=""
				style="display: none" />
		</form>

	</div>
	<jsp:include page="footer.jsp" />
</body>

</html>