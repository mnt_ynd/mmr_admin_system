<%@ page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品情報編集 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- カテゴリとジャンルのヒモづけ -->
<script type="text/javascript" src="js/categoryMatchGenre.js"></script>
<!-- fileをNoImageに置換 -->
<script type="text/javascript" src="js/noImage.js"></script>
<body>

	<!-- メニュー -->
	<jsp:include page="menu.jsp" />
	<div class="container">
		<h1>商品追加</h1>

		<form action="editItem" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="card">
						<div class="card-block">
							<div class="card-body">
								<div class="imgInput" style="width: 100%;">
									<input type="file" name="file1"> <img src="image/no_image.png" alt=""
										class="imgView" style="width: 100%;">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-8">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										商品番号
										<br>
										<input type="text" value="<c:out value="${item_list[0].itemId }" />" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										カテゴリ
										<br>
										<select class="category" name="category">
											<option value="">選択してください</option>
											<c:forEach var="cat" items="${requestScope.category_list}">
												<c:choose>
													<c:when test="${requestScope.item_list[0].categoryId == cat.categoryId}">
														<option value="<c:out value="${cat.categoryId}" />" selected><c:out
																value="${cat.categoryName}" /></option>
													</c:when>
													<c:otherwise>
														<option value="<c:out value="${cat.categoryId}" />"><c:out
																value="${cat.categoryName}" /></option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										ジャンル
										<br>
										<select class="genre" name="genre">
											<option value="" selected>選択してください</option>
											<c:forEach var="gen" items="${requestScope.genre_list}">
												<c:choose>
													<c:when test="${requestScope.item_list[0].genreId == gen.genreId}">
														<option value="<c:out value="${gen.genreId}" />" data-val="${gen.categoryId}" selected><c:out
																value="${gen.genreName}" /></option>
													</c:when>
													<c:otherwise>
														<option value="<c:out value="${gen.genreId}" />" data-val="${gen.categoryId}"><c:out
																value="${gen.genreName}" /></option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										ステータス
										<br>
										<select name="status">
											<option value="">選択してください</option>
											<c:forEach var="nao" items="${requestScope.new_and_old_list}">
												<option value="<c:out value="${nao.newAndOldId }" />"><c:out
														value="${nao.newAndOldName}" /></option>
											</c:forEach>
										</select>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										タイトル
										<br>
										<input type="text" name="title" placeholder="タイトル" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-8">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										アーティスト名(監督名)
										<br>
										<input type="text" name="artist" placeholder="アーティスト(監督名)" />
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										価格
										<br>
										<input type="number" name="price" min="0" max="99999" placeholder="価格" /> 円
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="card">
								<div class="card-block">
									<p class="card-text">
										備考
										<br>
										<textarea name="other" placeholder="備考" style="width: 100%;"></textarea>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<button type="submit" class="btn btn-primary btn-lg btn-block">追加</button>
			</div>
		</form>
	</div>

	<!-- footer -->
	<jsp:include page="footer.jsp" />
</body>
</html>