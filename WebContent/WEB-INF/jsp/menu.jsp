<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>分析 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- Webフォント -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="rental"><img src="image/logo.png" width="100px" /></a>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="rental"><i class="fa fa-home">
							貸出・返却処理</i></a></li>
				<c:if test="${manager_flg == 1}">
					<li class="nav-item"><a class="nav-link disabled" href="item"><i class="fa fa-cubes">
								商品メンテナンス</i></a></li>
					<li class="nav-item"><a class="nav-link disabled" href="empMaster"><i
							class="fa fa-users"> 従業員メンテナンス</i></a></li>
					<li class="nav-item"><a class="nav-link" href="analytics"><i class="fa fa-bar-chart">
								分析</i></a></li>
				</c:if>
				<li class="nav-item"><a class="nav-link" href="logout"><i class="fa fa-sign-out">
							ログアウト</i></a></li>
			</ul>
			<span class="navbar-text"><c:out value="${sessionScope.staff_name}" />さん</span>
		</div>
	</nav>
</body>
</html>