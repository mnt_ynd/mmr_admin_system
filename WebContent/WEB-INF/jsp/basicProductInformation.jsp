<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="jp.mnt.dto.CategoryDTO,java.util.ArrayList,jp.mnt.dto.GenreDTO,jp.mnt.dto.ItemDTO"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品マスタメンテナンス</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<!-- カテゴリとジャンルのヒモづけ -->
<script type="text/javascript" src="js/categoryMatchGenre.js"></script>
</head>


<script type="text/javascript" src="select.js" charset="Shift_JIS"></script>
<script type="text/javascript">
	function submitHandler(detail) {
		var form = document.forms["detailForm"];

		form.detail.value = detail;
		form.submit();

	}
</script>
<script type="text/javascript" src="recommded.js">

</script>
<body>
	<!-- メニュー -->
	<jsp:include page="menu.jsp" />

	<!-- 商品追加 -->
	<div class="container">
		<h1>商品マスタメンテナンス</h1>
		<c:if test="${!empty sessionScope.insert_item_message}">
			<p style="color: red;">
				<c:out value="${sessionScope.insert_item_message}" />
			</p>
			<c:remove var="insert_item_message" scope="session" />
		</c:if>
		<a href="newItem" class="btn btn-success" role="button">新規商品追加</a>
	</div>

	<!-- 商品検索 -->
	<div class="container">
		<h4>商品検索</h4>


		<form name="search" action="basicProductInformation" method="post">
			<select class="category" name="category">
				<option value="0" selected>選択してください</option>
				<c:forEach var="cat" items="${sessionScope.categoryList}">
					<option value="<c:out value="${cat.categoryId}" />"><c:out value="${cat.categoryName}" /></option>
				</c:forEach>
			</select> <select class="genre" name="genre" disabled>
				<option value="0" selected>選択してください</option>
				<c:forEach var="gen" items="${sessionScope.genreList}">
					<option value="<c:out value="${gen.genreId}" />" data-val="${gen.categoryId}"><c:out
							value="${gen.genreName}" /></option>
				</c:forEach>
			</select> <input type="text" name="item"> <input type="submit" value="検索">
		</form>
	</div>
	<br>
	<div class="container">
		<h4>検索結果</h4>
		<br>
		<%
			if (request.getAttribute("message") != null) {
		%>


		<%
			if (request.getAttribute("message").equals("")) {
		%>
		<c:forEach var="dto" items="${requestScope.ItemList}">
			<div class="card">
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<img style="width: 130px; height: 80px" src="getimage?id=<c:out value="${dto.itemId}" />"
							onerror="src='image/no_image.png'">
					</div>
					<div class="col-md-10 col-sm-10">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<c:out value="${dto.itemId}" />
							</div>
							<div class="col-md-4 col-sm-4">
								<c:out value="${dto.newAndOldName}" />
							</div>
							<div class="col-md-4 col-sm-4">
								<c:out value="${dto.categoryName }" />
							</div>
							<div class="col-md-9 col-sm-9">
								<c:out value="${dto.itemName}" />
							</div>
							<div class="col-md-3 col-sm-3">
								<form action="recommended" name="form1" method="post">
									<c:choose>
										<c:when test="${dto.recommendedFlg == 0}">
											<input type="hidden" name="i" value="<c:out value="${dto.itemId}" />" />
											<input type="hidden" name="r" value="<c:out value="${dto.recommendedFlg}" />" />
											<button type="submit" class="reco btn btn-success mx-auto" style="">おすすめ追加</button>
										</c:when>
										<c:otherwise>
											<input type="hidden" name="i" value="<c:out value="${dto.itemId}" />" />
											<input type="hidden" name="r" value="<c:out value="${dto.recommendedFlg}" />" />
											<button type="submit" class="reco btn btn-warning mx-auto">おすすめ解除</button>
										</c:otherwise>
									</c:choose>
								</form>
							</div>
							<div class="col-md-4 col-sm-4">
								<c:out value="${dto.artist}" />
							</div>
							<div class="col-md-4 col-sm-4">
								<c:out value="${dto.price}円" />
							</div>
							<div class="col-md-4 col-sm-4">
								<a type="button" class="btn btn-sm btn-primary"
									href="itemDetail?itemId=<c:out value="${dto.itemId}" />">詳細</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
		<%
			} else {
		%>
		<div><%=(String) request.getAttribute("message")%></div>
	</div>
	<%
		}
	%>
	<%
		}
	%>

	<jsp:include page="footer.jsp" />
</body>
</html>