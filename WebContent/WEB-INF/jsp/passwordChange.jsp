<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>パスワード変更 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>パスワード変更 - MMRオンライン -</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">

<!-- bootstrap3 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/loginStyle.css">

<!-- Webフォント -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<!-- パスワード強度チェッカー -->
<script src="js/passwordchecker.js" type="text/javascript"></script>
<script type="text/javascript">
	function setPasswordLevel(password) {
		var level = getPasswordLevel(password);
		var text = "";

		if (level == 1) {
			text = "弱い";
		}
		if (level == 2) {
			text = "やや弱い";
		}
		if (level == 3) {
			text = "普通";
		}
		if (level == 4) {
			text = "やや強い";
		}
		if (level == 5) {
			text = "強い";
		}

		document.getElementById("level").value = text;
		document.getElementById("level2").value = text;
	}
</script>
</head>
<title>パスワード設定 - MMR従業員システム</title>
<script type="text/javascript">
	function passwordChange() {
		if (fm.pass1.value == "" || fm.pass2.value == "") {
			alert("パスワードとパスワード(確認)を入力してください");
			return false;
		}
		return true;
	}
</script>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<div class="container">
		<h1>パスワード設定</h1>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<p>パスワードの設定が必要です。パスワードを設定してください</p>
			</div>
			<c:if test="${!empty requestScope.error_message}">
				<div class="col-md-12 col-sm-12">
					<p style="color: red;">
						<c:out value="${requestScope.error_message}" />
					</p>
					<c:remove var="error_message" scope="request" />
				</div>
			</c:if>
			<c:if test="${!empty requestScope.error_message2}">
				<div class="col-md-12 col-sm-12">
					<p style="color: red;">
						<c:out value="${requestScope.error_message2}" />
					</p>
					<c:remove var="error_message2" scope="request" />
				</div>
			</c:if>
			<div class="col-md-6 col-sm-6">
				<form action="passwordChange" method="post" name="fm">
					<fieldset class="form-group">
						<label for="pass1">新規パスワード</label> <input type="password" class="form-control" id="pass1"
							name="pass1" onkeyup="setPasswordLevel(this.value);" required autofocus><label
							for="pass1">パスワード強度</label> <input type="text" id="level" disabled style="width: 80px;">
						<input type="hidden" name="strength" id="level2" style="width: 80px;">
					</fieldset>
					<fieldset class="form-group">
						<label for="pass2">新規パスワード(確認)</label> <input type="password" class="form-control" id="pss2"
							name="pass2" required>
					</fieldset>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />

</body>
</html>