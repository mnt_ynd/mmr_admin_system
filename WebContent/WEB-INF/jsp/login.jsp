<%@page import="java.sql.Timestamp"%>
<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.mnt.dto.StaffDTO,jp.mnt.dto.RentalDetailDTO,java.util.ArrayList,java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-language" content="ja">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>レンタル一覧 -MMR従業員システム-</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HOME</title>
<!-- jQuery 3.1 -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<!-- bootstrap4 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="css/loginStyle.css">
<title>ログイン</title>
<script type="text/javascript" src="js/login.js"></script>
<script type="text/javascript">
	function login() {
		if (fm.id.value == "" || fm.pass.value == "") {
			alert("ログインIDとパスワードを入力してください");
			return false;
		}
		return true;
	}
</script>
</head>
<body>

	<div class="container">
		<div class="card card-container">
			<c:if test="${!empty requestScope.error_message}">
				<p style="color: red;">
					<c:out value="${requestScope.error_message}" />
				</p>
				<c:remove var="error_message" scope="request" />
			</c:if>
			<img id="profile-img" class="profile-img-card" src="image/logo1.png" />
			<p id="profile-name" class="profile-name-card"></p>
			<form class="form-signin" name="fm" action="login" method="post">
				<span id="reauth-email" class="reauth-email"></span> <input type="text" id="inputEmail"
					name="mail" class="form-control" placeholder="社員ID" required autofocus></input> <input
					type="password" id="inputPassword" name="pass" class="form-control" placeholder="Password"
					required></input>
				<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
			</form>
			<!-- /form -->
		</div>
		<!-- /card-container -->
	</div>
	<!-- /container -->
	<jsp:include page="footer.jsp" />
</body>
</html>